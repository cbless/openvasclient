/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.openvasclient.handler;

import de.cbless.openvasclient.model.responses.StopTaskResponse;
import java.io.IOException;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 *
 * @author christoph
 */
public class StopTaskResponseHandler extends DefaultHandler<StopTaskResponse>{

    public StopTaskResponseHandler() {
        super(new StopTaskResponse(), "stop_task_response");
    }

    @Override
    protected void parseStartElement(XMLStreamReader parser)  throws XMLStreamException, IOException  {
        
    }
    
}
