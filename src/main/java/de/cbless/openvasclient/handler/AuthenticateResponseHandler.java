/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.openvasclient.handler;

import de.cbless.openvasclient.model.responses.AuthenticateResponse;
import java.io.IOException;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 *
 * @author christoph
 */
public class AuthenticateResponseHandler extends DefaultHandler<AuthenticateResponse>{

    public AuthenticateResponseHandler() {
        super(new AuthenticateResponse(), "authenticate_response");
    }

    @Override
    protected void parseStartElement(XMLStreamReader parser)  throws XMLStreamException, IOException {
        if ("role".equals(parser.getName().toString())){
            response.setRole(parser.getElementText());
        }
        if ("timezone".equals(parser.getName().toString())){
            response.setTimezone(parser.getElementText());
        } 
    }
    
}
