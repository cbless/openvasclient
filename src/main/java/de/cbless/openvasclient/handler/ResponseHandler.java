/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.openvasclient.handler;

import de.cbless.openvasclient.OpenVASException;
import de.cbless.openvasclient.model.responses.Response;
import java.io.InputStream;

/**
 *
 * @author christoph
*/ 
public interface ResponseHandler<T extends Response> {

    String getStatusText();
    
    int getStatus();
    
    T getResponse();
    
    void parse(InputStream in) throws OpenVASException;
    
}
