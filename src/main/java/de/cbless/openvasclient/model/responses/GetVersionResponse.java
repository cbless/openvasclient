package de.cbless.openvasclient.model.responses;

public class GetVersionResponse extends Response {

	private String version;

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
	
	
}
