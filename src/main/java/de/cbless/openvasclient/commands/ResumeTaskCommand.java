/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.openvasclient.commands;

import de.cbless.openvasclient.DefaultCommand;
import de.cbless.openvasclient.handler.ResumeTaskResponseHandler;
import de.cbless.openvasclient.model.requests.ResumeTaskRequest;
import de.cbless.openvasclient.util.JAXBUtil;
import javax.xml.bind.JAXBException;

/**
 *
 * @author Christoph Bless
 */
public class ResumeTaskCommand extends DefaultCommand<ResumeTaskResponseHandler> {

    private ResumeTaskRequest request = new ResumeTaskRequest();

    public ResumeTaskCommand() {
        super("resume_task", true, new ResumeTaskResponseHandler());
    }
    
    
    public void setTaskId(String id){
        request.setTaskId(id);
    }
    

    @Override
    public String getRequest() throws JAXBException {
        return JAXBUtil.marshall(request, ResumeTaskRequest.class);
    }

}
