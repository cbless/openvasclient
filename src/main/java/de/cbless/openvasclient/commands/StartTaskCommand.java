/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.openvasclient.commands;

import de.cbless.openvasclient.DefaultCommand;
import de.cbless.openvasclient.handler.StartTaskResponseHandler;
import de.cbless.openvasclient.model.requests.StartTaskRequest;
import de.cbless.openvasclient.util.JAXBUtil;
import javax.xml.bind.JAXBException;

/**
 *
 * @author christoph
 */
public class StartTaskCommand extends DefaultCommand<StartTaskResponseHandler> {

    private StartTaskRequest request = new StartTaskRequest();

    public StartTaskCommand() {
        super("start_task", true, new StartTaskResponseHandler());
    }
    
    public void setTaskId(String id){
        request.setTaskId(id);
    }

    @Override
    public String getRequest() throws JAXBException {
        return JAXBUtil.marshall(request, StartTaskRequest.class);
    }

}
