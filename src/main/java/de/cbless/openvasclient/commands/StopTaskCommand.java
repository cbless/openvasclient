/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.openvasclient.commands;

import de.cbless.openvasclient.DefaultCommand;
import de.cbless.openvasclient.handler.StopTaskResponseHandler;
import de.cbless.openvasclient.model.requests.StopTaskRequest;
import de.cbless.openvasclient.util.JAXBUtil;
import javax.xml.bind.JAXBException;

/**
 *
 * @author christoph
 */
public class StopTaskCommand extends DefaultCommand<StopTaskResponseHandler> {

    private StopTaskRequest request = new StopTaskRequest();

    public StopTaskCommand() {
        super("stop_task", true, new StopTaskResponseHandler());
    }

    public void setTaskId(String id) {
        request.setTaskId(id);
    }

    @Override
    public String getRequest() throws JAXBException {
        return JAXBUtil.marshall(request, StopTaskRequest.class);
    }

}
