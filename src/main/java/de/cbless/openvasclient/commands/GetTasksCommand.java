/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.openvasclient.commands;

import de.cbless.openvasclient.DefaultCommand;
import de.cbless.openvasclient.handler.GetTasksResponseHandler;
import de.cbless.openvasclient.model.requests.GetTasksRequest;
import de.cbless.openvasclient.util.JAXBUtil;
import javax.xml.bind.JAXBException;

/**
 *
 * @author christoph
 */
public class GetTasksCommand extends DefaultCommand<GetTasksResponseHandler>{

    private GetTasksRequest request = new GetTasksRequest();

    public GetTasksCommand() {
        super("get_tasks", true, new GetTasksResponseHandler());
    }
    
    
    public void setUUID(String uuid){
        request.setTaskId(uuid);
    }
    public void setDetails(boolean details){
        request.setDetails(details);
    }
    

    @Override
    public String getRequest() throws JAXBException {
        return JAXBUtil.marshall(request, GetTasksRequest.class);
    }

}
