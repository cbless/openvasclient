/*
 * Copyright (C) 2015 Christoph Bless
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.cbless.openvasclient;

/**
 *
 * @author Christoph Bless
 */
public class TestConfiguration {
    public static final String USERNAME = "testuser";
    public static final String PASSWORD = "testuser";
    public static final String HOST = "127.0.0.1";
    public static final int PORT = 9390;
    
    public static final String NVT_OID = "1.3.6.1.4.1.25623.1.0.10330";
}
