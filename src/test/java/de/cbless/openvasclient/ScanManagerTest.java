/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.openvasclient;

import de.cbless.openvasclient.model.TaskStatus;
import de.cbless.openvasclient.model.resources.config.Config;
import de.cbless.openvasclient.model.resources.results.Result;
import de.cbless.openvasclient.model.resources.tasks.Task;
import de.cbless.openvasclient.model.responses.GetTasksResponse;
import de.cbless.openvasclient.model.responses.ResumeTaskResponse;
import de.cbless.openvasclient.model.responses.StartTaskResponse;
import de.cbless.openvasclient.model.responses.StopTaskResponse;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.in;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

/**
 *
 * @author christoph
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ScanManagerTest {
    private ScanManager scanManager;
    
    /**
     * @see http://docs.greenbone.net/GSM-Manual/gos-3.1/de/omp.html
     * 2xx: successfull
     * 200 = OK
     * 201 = Resource created
     * 202 = Request submitted
     * 
     * 4xx: Client error
     * 400 = Synthax Error
     * 401 = authentication required
     * 403 = Forbidden
     * 404 = Resource not found
     * 409 = resource busy
     * 
     * 5xx: Server error
     * 500 = Internal Error
     * 503 = Service temporarily down / serive unavailable
     * 
     * 
     */
    private List<Integer> knownStatusCodes = Arrays.asList(new Integer[]{
        200, 201, 202, 400, 401, 403, 404, 409, 500, 503});
            
        

    public ScanManagerTest() throws IOException, KeyManagementException, 
            NoSuchAlgorithmException{
        ClientBuilder builder = new ClientBuilder(TestConfiguration.HOST, 9390, true);
        OMPClient client = builder.build();
        client.setUsername(TestConfiguration.USERNAME);
        client.setPassword(TestConfiguration.PASSWORD);
        scanManager = new ManagerFactory(client).createScanManager();
        
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    private String getScanId() throws OpenVASException{
        try{
            GetTasksResponse response = scanManager.getTasks();
        }catch(Exception ex ){
            ex.printStackTrace();
        }
        GetTasksResponse response = scanManager.getTasks();
        if (response.getTasks().size() > 0){
            String id = response.getTasks().get(0).getId();
            return id;
        } 
        throw new IllegalStateException("At least one Task is required in "
                + "your OpenVAS installation");
    }
    @Test
    public void testGetTasks() throws OpenVASException {
        GetTasksResponse response = scanManager.getTasks();
        if (response.getTasks().size() > 0){
            for (Task t : response.getTasks()){
                assertNotNull(t.getId());
                assertNotNull(t.getName());
            }
        }
    }
    
    
    @Test
    public void testGetTasksUUID() throws OpenVASException {
        String id = getScanId();
        GetTasksResponse response = scanManager.getTasks(id);
        assertTrue(response.getTasks().size() == 1);
        assertThat(id, equalTo(response.getTasks().get(0).getId()));
    }
    
    /**
     * Test of startTask method, of class ScanManager.
     */
    @Test
    public void test1StartTask() throws Exception {
        System.out.println("startTask");
        String id = getScanId();
        try{
            StartTaskResponse response = scanManager.startTask(id);
            assertNotNull(response.getReportId());
            int status = response.getStatus();
            assertThat(status, in(knownStatusCodes));
        } catch(StatusException ex){
            fail(ex.getMessage());
        }
        Thread.sleep(5000);        
    }

    /**
     * Test of stopTask method, of class ScanManager.
     */
    @Test
    public void test2StopTask() throws Exception {
        System.out.println("stopTask");
        String id = getScanId();
        StopTaskResponse response = scanManager.stopTask(id);
        int status = response.getStatus();
        assertThat(status, in(knownStatusCodes));
        Thread.sleep(5000);        
    }

    /**
     * Test of resumeTask method, of class ScanManager.
     */
    @Test
    public void test3ResumeTask() throws Exception {
        System.out.println("resumeTask");
        String id = getScanId();
        try{
            boolean running = true;
            while (running){
                String status = scanManager.getTaskStatus(id);
                if (TaskStatus.StopRequested.getStatus().equals(status)){
                    System.out.println("waiting until task reached the required state 'stopped'");
                    Thread.sleep(5000);
                    continue;
                } else if (TaskStatus.Stopped.getStatus().equals(status)){
                    break;
                } else {
                    fail("Can not resume task, while it is in status '" + status + "'");
                }
            }
            ResumeTaskResponse response = scanManager.resumeTask(id);
            int status = response.getStatus();
            assertThat(status, in(knownStatusCodes));
        }catch(StatusException ex){
            fail(ex.getMessage());
        }
    }
    
    @Test
    public void testGetResults() throws OpenVASException {
        String id = getScanId();
        List<Result> results = scanManager.getResults(id);  
        System.out.println(results);
        if (results.size() > 0 ){
            for ( Result r : results){
                assertNotNull(r.getId());
                assertNotNull(r.getName());
            }
        }
    }
    
    
    @Test
    public void testGetConfig() throws OpenVASException {
        List<Config> results = scanManager.getConfigs();
        for (Config c : results){
            assertNotNull(c.getId());
            assertNotNull(c.getName());
        }
    }
}
