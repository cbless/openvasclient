/*
 * Copyright (C) 2015 Christoph Bless
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.cbless.openvasclient;

import de.cbless.openvasclient.model.resources.Target;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Christoph Bless
 */
public class ConfigManagerTest {
    
    private ConfigManager configManager;
    
    public ConfigManagerTest() throws IOException, KeyManagementException, 
            NoSuchAlgorithmException {
        ClientBuilder builder = new ClientBuilder(TestConfiguration.HOST, 9390, true);
        OMPClient client = builder.build();
        client.setUsername(TestConfiguration.USERNAME);
        client.setPassword(TestConfiguration.PASSWORD);
        configManager = new ManagerFactory(client).createConfigManager();
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getTargets method, of class ConfigManager.
     */
    @Test
    public void testGetTargets() throws Exception {
        System.out.println("getTargets");
        List<Target> targets = configManager.getTargets();
        for (Target t : targets){
            assertNotNull(t.getId());
            assertNotNull(t.getName());
        }
    }
    
    @Test
    public void testGetTargets_arg() throws Exception {
        System.out.println("getTargets");
        List<Target> targets = configManager.getTargets();
        if (targets.isEmpty()){
           fail("you have to configure at least one target");
        }
        Target expected = targets.get(0);
        
        List<Target> t2 = configManager.getTargets(expected.getId());
        if (targets.isEmpty()){
            fail("Target not found");
        } else {
            for (Target t : t2){
                assertTrue(t.getId().equals(expected.getId()));
                assertTrue(t.getName().equals(expected.getName()));
            }
        }
    }
}
