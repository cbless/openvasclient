/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.cbless.openvasclient;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author christoph
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class OMPClientTest {
    
    private OMPClient client;
    
    public OMPClientTest() throws IOException, KeyManagementException, 
            NoSuchAlgorithmException{
        ClientBuilder builder = new ClientBuilder(TestConfiguration.HOST, 9390, true);
        client = builder.build();
        
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    
    /**
     * Test of the isAuthenticated and authenticate method.
     */
    @Test
    public void testAll() throws OpenVASException {
        System.out.println("test");
        assertFalse(client.isAuthenticated());
        try{
            client.authenticate();
        } catch(LoginException ex){
            // This is OK because the username and password isn't set
        }
        assertFalse(client.isAuthenticated());
        
        
        client.setUsername(TestConfiguration.USERNAME);
        client.setPassword(TestConfiguration.PASSWORD);
        try{
            client.authenticate();
        } catch(LoginException ex){
            // This time it is an error because the username an password was set
            fail(ex.getMessage());
        }
        assertTrue(client.isAuthenticated());
        
    }

}
