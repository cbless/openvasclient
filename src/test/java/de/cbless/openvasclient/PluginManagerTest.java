/*
 * Copyright (C) 2015 Christoph Bless
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.cbless.openvasclient;

import de.cbless.openvasclient.model.resources.NVT;
import de.cbless.openvasclient.model.resources.NvtFamily;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.equalTo;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Christoph Bless
 */
public class PluginManagerTest {
    
    private final PluginManager manager;
    
    public PluginManagerTest() throws IOException, KeyManagementException, 
            NoSuchAlgorithmException{
        ClientBuilder builder = new ClientBuilder(TestConfiguration.HOST, 
                TestConfiguration.PORT, true);
        OMPClient client = builder.build();
        client.setUsername(TestConfiguration.USERNAME);
        client.setPassword(TestConfiguration.PASSWORD);
        manager = new ManagerFactory(client).createPluginManager();
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getNVTs method, of class PluginManager.
     */
    @Test
    public void testGetNVTs() throws OpenVASException {
        System.out.println("getNVTs");
        List<NVT> nvts = manager.getNVT(TestConfiguration.NVT_OID);
        assertTrue(nvts.size() == 1);
        assertThat(TestConfiguration.NVT_OID , equalTo(nvts.get(0).getOid()));
    }
    
    
    @Test
    public void testGetNvtFamilies() throws OpenVASException {
        System.out.println("getNvtFamilies");
        List<NvtFamily> families = manager.getFamilies();
        for (NvtFamily f : families){
            assertNotNull(f.getName());
            assertTrue(f.getNvtCount() >= 0);
        }
    }
}
